const fs = require('fs')
const length = 1000
const interval = 1000 * 60 * 60 * 24
const start = Date.now() - interval * length
const chartKeys = ['pink', 'cherry', 'red', 'blue', 'black']
const chart = chartKeys.reduce((acc, key) => {
  acc[key] = generateSingleTrend({ length, start, interval })
  return acc
}, {})

fs.writeFile('src/chart.json', JSON.stringify(chart), function () {
  console.log('successfully saved src/chart.json')
})

function generateSingleTrend ({ start, interval, length }) {
  const trend = []
  let prevValue = 0
  for (let i = 0; i < length; i++) {
    const value = Math.max(prevValue + Math.floor(10 * (Math.random() - .5)), 0)
    trend.push({
      time: start + i * interval,
      value
    })
    prevValue = value
  }
  return trend
}