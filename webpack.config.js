const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const dist = path.resolve(__dirname, 'public')

module.exports = function (argc, argv) {
  const config = {
    context: path.resolve(__dirname, 'src'),
    devServer: {
      contentBase: dist
    },
    entry: './index.js',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            plugins: [
              '@babel/plugin-transform-spread',
              '@babel/plugin-proposal-object-rest-spread'
            ]
          }
        },
        {
          type: 'javascript/auto',
          test: /\.json$/,
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]'
          }
        }
      ]
    },
    output: {
      path: dist
    },
    plugins: [
      new HtmlWebpackPlugin({ template: './index.html'})
    ]
  }
  if (argv.mode === 'production') {
    config.devtool = 'source-map'
  }
  return config  
}