# Charts

Links to useful materials
- https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Optimizing_canvas
- https://www.youtube.com/watch?v=wkDd-x0EkFU
- https://www.youtube.com/watch?v=NG5uDXCOr8s
- https://www.youtube.com/watch?v=dlZvL7Ei0C0

## TODO

- ~~X Axis~~
- ~~Y Axis~~
- Draggable range styles & perfect drag behavior
- ~~Overlay showing values for each x~~
- ~~CSS styles~~, resizing to desktop
- Non webgl solution for like IE9+ at least
- poor scale canvas performance on Android
