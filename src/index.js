import Chart from './Chart'
import './chart_data.json'

document.addEventListener('DOMContentLoaded', () => {
  const renderContainer = document.querySelector('[data-render-container]')
  const chartTemplate = document.querySelector('[data-chart-template]')
  const xhr = new XMLHttpRequest()
  xhr.addEventListener('load', function() {
    const json = JSON.parse(this.response)
    const charts = json.map((el) => prepareData(el))
      .map(({ trends, timestamps }) => {
        const wrapper = document.createElement('div')
        renderContainer.appendChild(wrapper)
        wrapper.innerHTML = chartTemplate.innerHTML
        wrapper.className = 'chart-wrapper'
        return new Chart({ wrapper, trends, timestamps })
      })
    animate()
    function animate() {
      charts.forEach(chart => chart.renderIfNecessary())
      requestAnimationFrame(animate)
    }
  })
  xhr.open('GET', 'chart_data.json')
  xhr.send()
  const nightModeSwitch = document.querySelector('[data-night-mode-switch]')
  nightModeSwitch.addEventListener('click', () => {
    document.documentElement.classList.toggle('night')
    if (document.documentElement.classList.contains('night')) {
      nightModeSwitch.innerHTML = 'Switch to day mode'
    } else {
      nightModeSwitch.innerHTML = 'Switch to night mode'
    }
  })
})

function prepareData (chartData) {
  const trendsKeys = Object.keys(chartData.types).filter(key => chartData.types[key] === 'line')
  const mapTrendsToValues = chartData.columns.reduce((acc, [trendKey, ...values]) => ({
    ...acc,
    [trendKey]: values
  }), {})
  const trends = trendsKeys.map((trendKey, i) => {
    const values = mapTrendsToValues[trendKey]
    const zippedXY = []
    values.forEach((el, i) => {
      zippedXY.push(i)
      zippedXY.push(el)
    })
    const vertices = new Float32Array(zippedXY)
    const color = chartData.colors[trendKey]
    return {
      color,
      colorVector3: hexToRgbArray(color),
      values,
      vertices,
      name: chartData.names[trendKey],
      enabled: true,
      opacity: 1,
      zIndex: 2 + i
    }
  }, {})
  const timestamps = mapTrendsToValues['x']
  return { trends, timestamps }
}

function hexToRgbArray (hex) {
  const noHashHex = hex.replace('#','')
  const r = parseInt(noHashHex.substring(0,2), 16)
  const g = parseInt(noHashHex.substring(2,4), 16)
  const b = parseInt(noHashHex.substring(4,6), 16)
  return [r,g,b].map(el => el / 256)
}
