import draggable from './draggable'
import setupWebglProgram from './setupWebglProgram'
import toggles from './toggles'
import XAxis from './XAxis'
import YAxis from './YAxis'
import Magnifier from './Magnifier'
import addPrefix from './addPrefix'
import formatDate from './formatDate'

export default class Chart {
  constructor ({ wrapper, trends, timestamps }) {
    this.wrapper = wrapper
    this.mainCanvas = wrapper.querySelector('[data-main-canvas]')
    this.dpr = window.devicePixelRatio || 1
    this.resizeCanvas(this.mainCanvas)
    this.gl = this.mainCanvas.getContext('experimental-webgl')
    this.program = setupWebglProgram(this.gl)
  
    this.trends = trends
    this.trendLength = this.trends[0].values.length - 1
    this.scale = { leftPosition: 0, rightPosition: 1 }

    draggable(wrapper, ({ leftPosition, rightPosition }) => {
      this.scale.leftPosition = leftPosition
      this.scale.rightPosition = rightPosition
      this.adjustScaleY()
    })
  
    toggles(wrapper, trends, (trend, checked) => {
      trend.enabled = checked
      this.adjustScaleY()
    })
    
    this.axisCanvas = this.wrapper.querySelector('[data-axis-canvas]')
    this.resizeCanvas(this.axisCanvas)
    this.axisContext = this.axisCanvas.getContext('2d')
    this.axisContext.font = `${10 * this.dpr}pt arial`
    this.yAxis = new YAxis()
    this.xAxis = new XAxis(timestamps)
    this.adjustScaleY(true)

    this.scaleCanvas = wrapper.querySelector('[data-scale-canvas]')
    this.resizeCanvas(this.scaleCanvas)
    this.scaleContext = this.scaleCanvas.getContext('2d')
    this.scaleContext.lineWidth = 2
    this.renderScale()

    this.magnifier = new Magnifier(wrapper, trends, (x) => {
      const { translateX, scaleX } = this.getTransforms()
      const index = Math.round(this.trendLength * (x - translateX) / scaleX)
      this.setSelectedDate(index)
    })
    this.setSelectedDate(0)
  }

  setSelectedDate (i) {
    this.magnifier.setDate(
      formatDate(this.xAxis.timestamps[i].timestamp, null, { week: true }),
      this.trends.map(({ values }) => addPrefix(values[i]))
    )
  }

  resizeCanvas (canvas) {
    canvas.width = this.wrapper.clientWidth * this.dpr
    canvas.height = this.wrapper.clientHeight * this.dpr
    return canvas
  }

  adjustScaleY (isInit) {
    const { leftPosition, rightPosition } = this.scale
    const cutStartIndex = Math.floor(leftPosition * this.trendLength)
    const cutEndIndex = Math.ceil(rightPosition * this.trendLength)
    const supremum = this.getSupremum(cutStartIndex, cutEndIndex)
    this.targetScaleY = 1 / supremum
    this.trendWideScaleY = 1 / this.getSupremum()
    this.needsRender = true
    if (isInit) {
      this.yAxis.addRange({ supremum, opacity: 1 })
      this.xAxis.addRange({ cutStartIndex, cutEndIndex, opacity: 1 })
      this.animatedScaleY = this.targetScaleY
      this.animatedTrendWideScaleY = this.trendWideScaleY
    } else {
      this.yAxis.addRange({ supremum })
      this.xAxis.addRange({ cutStartIndex, cutEndIndex })
    }
  }

  getSupremum(cutStartIndex = null, cutEndIndex) {
    const enabledTrends = this.trends.filter(({ enabled }) => enabled)
    const trendsToCheck = enabledTrends.length === 0 ? this.trends : enabledTrends
    return trendsToCheck.map(({ values }) => values)
      .reduce((acc, singleTrend) => {
        const selectedRange = cutStartIndex !== null ? singleTrend.slice(cutStartIndex, cutEndIndex)
          : singleTrend
        return Math.max(acc, selectedRange.reduce((acc2, curr) => Math.max(acc2, curr), 0))
      }, 0)
  }

  renderIfNecessary () {
    const delta = Math.abs(this.animatedScaleY - this.targetScaleY)
    if (delta > 1e-3 * this.targetScaleY) {
      this.needsRender = true
      this.animatedScaleY += .16 * (this.targetScaleY - this.animatedScaleY)
    } else if (delta > 0) {
      this.animatedScaleY = this.targetScaleY
    }
    const trendWideDelta = Math.abs(this.animatedTrendWideScaleY - this.trendWideScaleY)
    if (trendWideDelta > 1e-3 * this.trendWideScaleY) {
      this.animatedTrendWideScaleY += .16 * (this.trendWideScaleY - this.animatedTrendWideScaleY)
      this.renderScale()
    } else if (delta > 0) {
      this.animatedTrendWideScaleY = this.trendWideScaleY
    }
    this.trends.forEach((trend) => {
      const { enabled, opacity } = trend
      if (enabled && opacity < 1) {
        trend.opacity = Math.min(opacity + .05, 1)
        this.needsRender = true
        this.renderScale()
      } else if (!enabled && opacity > 0) {
        trend.opacity = Math.max(opacity - .05, 0)
        this.needsRender = true
        this.renderScale()
      }
    })
    if (this.needsRender || this.yAxis.rangeQueue.needsRender) {
      this.renderYAxis()
    }
    if (this.needsRender || this.xAxis.rangeQueue.needsRender) {
      this.renderXAxis()
    }
    if (this.needsRender) {
      this.render()
      this.needsRender = false
    }
  }

  getTransforms() {
    const { leftPosition, rightPosition } = this.scale
    const scaleX = 1 / (rightPosition - leftPosition)
    const translateX = -leftPosition * scaleX
    return { translateX, scaleX }
  }

  render () {
    const { translateX, scaleX } = this.getTransforms()
    const { gl, program } = this

    program.scale = gl.getUniformLocation(program, 'scale')
    gl.uniform3fv(program.scale, [scaleX / this.trendLength, this.animatedScaleY, 0])

    this.trends.forEach((trend) => {
      const { colorVector3, vertices, opacity, zIndex } = trend
      const buffer = gl.createBuffer()
      gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
      gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW)

      program.translate = gl.getUniformLocation(program, 'translate')
      gl.uniform3fv(program.translate, [translateX, 0, 1 - .01 * zIndex])

      program.color = gl.getUniformLocation(program, 'color')
      gl.uniform4fv(program.color, [...colorVector3, opacity])

      program.position = gl.getAttribLocation(program, 'position')
      gl.enableVertexAttribArray(program.position)
      gl.vertexAttribPointer(program.position, 2, gl.FLOAT, false, 0, 0)

      gl.drawArrays(gl.LINE_STRIP, 0, vertices.length / 2)
    })
  }

  renderYAxis () {
    const ctx = this.axisContext
    ctx.save()
    ctx.clearRect(0, 0, this.axisCanvas.width, this.axisCanvas.height)
    this.yAxis.rangeQueue.animationFrame()
    this.yAxis.rangeQueue.ranges.forEach(({ opacity, labels, payload: { supremum } }) => {
      const scaleFactor = supremum * this.animatedScaleY
      labels.forEach(({ notch, label }) => {
        const y = Math.floor((1 - scaleFactor * notch) * (this.axisCanvas.height - 20))
        ctx.fillStyle = `rgba(150,	162,	170, ${opacity})`
        ctx.fillText(label, 0, y)
      })
    })
    ctx.restore()
  }

  renderXAxis () {
    const { translateX, scaleX } = this.getTransforms()
    const ctx = this.axisContext
    ctx.save()
    ctx.clearRect(0, this.axisCanvas.height - 20, this.axisCanvas.width, this.axisCanvas.height)
    this.xAxis.rangeQueue.animationFrame()
    this.xAxis.rangeQueue.ranges.forEach(({ labels, opacity }) => {
      labels.forEach(({ label, position }) => {
        const x = Math.floor((position * scaleX + translateX) * this.axisCanvas.width)
        ctx.fillStyle = `rgba(150,	162,	170, ${opacity})`
        ctx.fillText(label, x - 17, this.axisCanvas.height)
      })
    })
    ctx.restore()
  }

  renderScale () {
    const { width, height } = this.scaleCanvas
    const ctx = this.scaleContext
    ctx.save()
    ctx.clearRect(0, 0, width, height)
    this.trends.forEach((trend) => {
      const { colorVector3, vertices, opacity } = trend
      ctx.moveTo(0, Math.ceil(vertices[1] * height))
      ctx.beginPath()
      for (let i = 0; i < vertices.length; i += 2) {
        const x = 2 * vertices[i] / vertices.length
        const y = 1 - this.animatedTrendWideScaleY * vertices[i + 1]
        ctx.lineTo(Math.ceil(x * width), Math.ceil(y * height))
      }
      ctx.strokeStyle = rgba(colorVector3, opacity)
      ctx.stroke()
      ctx.restore()
    })
  }
}

function rgba (vector3, opacity) {
  const colors = vector3.map(x => Math.ceil(255 * x)).join(',')
  return `rgba(${colors}, ${opacity})`
}