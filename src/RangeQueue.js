export default class RangeQueue {
  constructor () {
    this.ranges = []
    this.needsRender = false
    this.isSpeedUp = false
  }

  addRange ({ opacity = 0, labels, payload }) {
    this.ranges.map(range => range.isLeaving = true)
    this.ranges.push({ isLeaving: false, opacity, labels, payload })
    this.needsRender = true
  }

  speedUpCurrentTransition () {
    this.isSpeedUp = true
  }

  animationFrame () {
    let opacity
    let needsRender = false
    let speed = this.isSpeedUp ? 1 : .05
    this.ranges.forEach((range) => {
      if (range.isLeaving) {
        opacity = Math.max(range.opacity - speed, 0)
      } else {
        opacity = Math.min(range.opacity + speed, 1)
      }
      if (opacity !== range.opacity) {
        needsRender = true
        range.opacity = opacity
      }
    })
    this.ranges = this.ranges.filter(({ opacity }) => opacity > 0)
    this.needsRender = needsRender
    if (!needsRender && this.isSpeedUp) {
      this.isSpeedUp = false
    }
  }
}
