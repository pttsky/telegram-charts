export default class {
  constructor (wrapper, trends, onUpdate) {
    const magnifier = wrapper.querySelector('[data-hover-magnifier]')
    const listener = wrapper.querySelector('[data-magnifier-listener]')
    this.bar = wrapper.querySelector('[data-magnifier-bar]')
    const date = magnifier.querySelector('[data-magnifier-date]')
    const trendsWrapper = magnifier.querySelector('[data-magnifier-trends-wrapper]')
    this.nodes = trends.map(({ color, name }) => {
      const element = document.createElement('div')
      element.style.color = color
      element.className = 'magnifier-number'
      const number = document.createElement('h3')
      const numberNode = document.createTextNode('0')
      const title = document.createElement('div')
      const titleNode = document.createTextNode(name)
      title.appendChild(titleNode)
      title.className = 'magnifier-number__title'
      number.appendChild(numberNode)
      number.className = 'magnifier-number__number'
      element.appendChild(number)
      element.appendChild(title)
      trendsWrapper.appendChild(element)
      return numberNode
    })
    this.dateNode = document.createTextNode(Date.now())
    date.appendChild(this.dateNode)
    this.onUpdate = onUpdate
    magnifier.addEventListener('mousemove', e => e.stopPropagation())
    magnifier.addEventListener('touchmove', e => e.stopPropagation())
    listener.addEventListener('mousemove', (e) => {
      this.update(e.offsetX / listener.clientWidth)
    })
    listener.addEventListener('touchmove', (e) => {
      const touch = e.targetTouches[0]
      this.update(touch.offsetX / listener.clientWidth)
    })
    this.element = magnifier
    this.listenerElement = listener
  }

  setDate (date, pointValues) {
    this.dateNode.textContent = date
    pointValues.forEach((value, i) => this.nodes[i].textContent = value)
  }

  update (position) {
    this.onUpdate(position)
    this.bar.style.left = `${position * 100}%`
    if ((this.listenerElement.clientWidth - 20) * (1 - position) < this.element.clientWidth) {
      this.element.style.left = ''
      this.element.style.right = '0'
    } else if (20 / this.listenerElement.clientWidth > position) {
      this.element.style.right = ''
      this.element.style.left = '0'
    } else {
      this.element.style.right = ''
      this.element.style.left = `calc(${position * 100}% - 20px)`
    }
  }
}