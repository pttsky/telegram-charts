export default function (wrapper, onUpdate) {
  const scaleListener = wrapper.querySelector('[data-scale-listener]')
  const leftHandle = wrapper.querySelector('[data-left-handle]')
  const rightHandle = wrapper.querySelector('[data-right-handle]')
  let leftPosition = 0
  let rightPosition = 1
  const listenerWidth = scaleListener.clientWidth
  let isDown = false
  scaleListener.addEventListener('mousedown', (e) => {
    adjustByPageX(e.pageX)
    isDown = true
  }, false)
  document.addEventListener('mousemove', (e) => {
    if (isDown) {
      adjustByPageX(e.pageX)
    }
  }, false)
  scaleListener.addEventListener('touchmove', (e) => {
    const touch = event.targetTouches[0]
    adjustByPageX(touch.pageX)
  }, false)
  document.addEventListener('mouseup', () => {
    isDown = false
  }, false)

  function adjustByPageX(pageX) {
    const ratio = (pageX - scaleListener.offsetLeft) / listenerWidth
    const normedRatio = Math.max(0, Math.min(1, ratio))
    adjustScale(normedRatio)
  }

  function adjustScale(ratio) {
    const distanceRight = Math.abs(rightPosition - ratio)
    const distanceLeft = Math.abs(leftPosition - ratio)
    if (distanceLeft < 0.05 && distanceRight < 0.05) {
      return
    }
    const isRightNearest = distanceLeft > distanceRight
    if (isRightNearest) {
      rightPosition = ratio
      rightHandle.style.transform = `translate(${Math.round((ratio - 1) * listenerWidth)}px, 0)`
    } else {
      leftPosition = ratio
      leftHandle.style.transform = `translate(${Math.round(ratio * listenerWidth)}px, 0)`
    }
    if (onUpdate) {
      onUpdate({ leftPosition, rightPosition })
    }
  }
}
