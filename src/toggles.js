export default function (wrapper, trends, onChange) {
  const trendsTogglesContainer = wrapper.querySelector('[data-trends-toggles-container]')
  trends.forEach((trend) => {
    const label = document.createElement('label')
    const input = document.createElement('input')
    const circle = document.createElement('div')
    circle.className = 'toggle__circle'
    circle.innerHTML = '<svg viewBox="0 0 16 16"><path d="M3,8 L7,12 L13,5" stroke="white" stroke-width="2" fill="none"></path></svg>'
    const span = document.createElement('span')
    span.innerHTML = trend.name
    span.className = 'toggle__label'
    input.setAttribute('type', 'checkbox')
    if (trend.enabled) {
      input.setAttribute('checked', '')
    }
    input.addEventListener('change', function () {
      onChange(trend, input.checked)
    })
    label.appendChild(input)
    label.appendChild(circle)
    label.appendChild(span)
    label.className = 'toggle'
    label.style.color = trend.color
    trendsTogglesContainer.appendChild(label)
  })
}
