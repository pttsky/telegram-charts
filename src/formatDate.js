const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
const weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']

export default function formatDate (timestamp, daySpanPerNotch, config = {}) {
  const { week = false } = config
  const d = new Date(timestamp)
  const mm = months[d.getMonth()]
  const dd = d.getDate()
  const yy = d.getFullYear()
  if (week) {
    const ww = weekDays[d.getDay()]
    return ww + ', ' + dd + ' ' + mm
  } else if (daySpanPerNotch > 365) {
    return yy
  } else if (daySpanPerNotch > 30) {
    return mm + ' ' + yy
  } else {
    return mm + ' ' + dd
  }
}