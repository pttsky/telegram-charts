import RangeQueue from './RangeQueue'
import addPrefix from './addPrefix'

export default class YAxis {
  constructor () {
    this.minimalChange = 1.05
    this.rangeQueue = new RangeQueue()
    this.numberOfNotches = 5
    this.step = 1 / (this.minimalChange ** 2 * this.numberOfNotches)
    this.notches = []
    for (let i = 0; i < this.numberOfNotches; i++) {
      this.notches.push((i + 1) * this.step)
    }
    this.rangeLength = 0
  }

  addRange ({ supremum, opacity = 0 }) {
    const newRangeLength = supremum
    if (newRangeLength === 0) {
      throw new Error('Attempt to add zero length range')
    }
    const change = Math.max(newRangeLength, this.rangeLength) / Math.min(newRangeLength, this.rangeLength)
    if (change > this.minimalChange) {
      this.rangeQueue.addRange({
        opacity,
        payload: { supremum },
        labels: this.notches.map((notch) => ({
          notch,
          label: addPrefix(Math.floor(notch * supremum))
        })),
      })
      this.rangeLength = newRangeLength
    }
  }
}
