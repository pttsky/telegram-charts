export default function addPrefix (number) {
  if (number > 1e9) {
    return (number * 1e-9).toFixed(1) + 'G'
  } else if (number > 1e6) {
    return (number * 1e-6).toFixed(1) + 'M'
  } else if (number > 1e3) {
    return (number * 1e-3).toFixed(1) + 'K'
  } else {
    return number + ''
  }
}
