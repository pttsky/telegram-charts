
export default function (gl) {
  const dpr = window.devicePixelRatio || 1
  const vertexShader = createShader(gl, gl.VERTEX_SHADER, `
    attribute vec2 position;
    uniform vec3 scale;
    uniform vec3 translate;
    void main() {
      vec3 normalizedResult = vec3(position, 0.0) * scale + translate;
      gl_Position = vec4(2.0 * normalizedResult - 1.0, 1.0);
    }
  `)
  const fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, `
    precision mediump float;
    uniform vec4 color;
    void main() {
      gl_FragColor = color;
      gl_FragColor.rgb *= gl_FragColor.a;
    }
  `)
  const program = gl.createProgram()
  gl.attachShader(program, vertexShader)
  gl.attachShader(program, fragmentShader)
  gl.linkProgram(program)
  gl.useProgram(program)
  gl.lineWidth(2 * dpr)
  gl.enable(gl.BLEND)
  gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
  return program
}

function createShader (gl, type, codeString) {
  const shader = gl.createShader(type)
  gl.shaderSource(shader, codeString)
  gl.compileShader(shader)
  return shader 
}
