import RangeQueue from './RangeQueue'
import formatDate from './formatDate'

const dayLength = 1000 * 60 * 60 * 24

export default class XAxis {
  constructor (timestamps) {
    this.rangeQueue = new RangeQueue()
    const length = timestamps.length
    this.timestamps = timestamps.map((timestamp, i) => ({
      timestamp,
      position: i / length
    }))
    this.daySpanPerNotch = 0
    this.numberOfNotches = 5
  }

  addRange({ cutStartIndex, cutEndIndex, opacity }) {
    const timeSpan = this.timestamps[cutEndIndex - 1].timestamp - this.timestamps[cutStartIndex].timestamp
    const daySpanPerNotch = Math.max(1, Math.ceil(timeSpan / (dayLength * this.numberOfNotches)))
    if (daySpanPerNotch !== this.daySpanPerNotch) {
      const labels = []
      for (let i = Math.ceil(0.5 * daySpanPerNotch); i < this.timestamps.length; i += daySpanPerNotch) {
        const label = formatDate(this.timestamps[i].timestamp, daySpanPerNotch)
        labels.push({ position: this.timestamps[i].position, label })
      }
      this.rangeQueue.addRange({ opacity, labels })
      if (daySpanPerNotch / this.daySpanPerNotch > 1.25) {
        this.rangeQueue.speedUpCurrentTransition()
      }
      this.daySpanPerNotch = daySpanPerNotch
    }
  }
}

